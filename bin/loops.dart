void main(List<String> arguments) {
  List<int> listOfInts = [1, 2, 3, 4, 5, 6, 7, 1, 1, 3, 5, 9];

  //for

  void showForLoop(list) {
    for (int i = 0; i < list.length; i++) {
      print(list[i]);
    }
  }
  showForLoop(listOfInts);

  //forIn

  void showForInLoop (list) {
    for (var item in list){
      print('Index of $item is: ${list.indexOf(item)}');
    }
  }
  showForInLoop(listOfInts);

  //forEach

    void showForEachLoop (list) {
      print('forEach:');
      list.forEach((i) {
      i++;
    });
  }
  showForEachLoop(listOfInts);

  //while

  showWhileLoop(int a, int b) {
    while (a <= b) {
      print('Current while iteration: ${a}');
      a++;
    }
  }
  showWhileLoop(0, 5);


  //do while
  void  showDoWhileLoop() {
    int doWhileCondition = 0;

    do {
      print('In do-while loop do-block will be executed anyway at least one time');
      doWhileCondition++;
    } while (doWhileCondition < 2);
  }
  showDoWhileLoop();

  //map

  void showMapping (List <int> list) {
    var newMultipliedList = list.map((item) => item*2).toList();
    print('Multiplied list: $newMultipliedList');
  }
  showMapping(listOfInts);

}
